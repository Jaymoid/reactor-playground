package james

import reactor.core.publisher.Flux
import reactor.core.scheduler.Schedulers
import java.time.Duration
import java.time.Instant
import java.util.*

/**

1. only use singles. (comment out log and publish on parallel)

incomingEventFlux
.onBackpressureBuffer()
.flatMap { incomingEvent ->
enrich(incomingEvent)
// .subscribeOn(Schedulers.parallel())
}
.flatMap { enrichedMsg ->
writeToOutgoing(enrichedMsg)
.flatMap { commitOriginalOffset(enrichedMsg) }
}
//.log()
.filter { it.id % skipCount == 0L }
.subscribe { printStats(it, startTime) }

Average: 34ms per req


2. USe all the cores:

incomingEventFlux
.onBackpressureBuffer()
//            .publishOn(Schedulers.single())
.flatMap { incomingEvent ->
enrich(incomingEvent)
//                        .publishOn(Schedulers.single())
.subscribeOn(Schedulers.parallel())
}
.flatMap { enrichedMsg ->
writeToOutgoing(enrichedMsg)
.flatMap { commitOriginalOffset(enrichedMsg) }
}
// .log()
.filter { it.id % skipCount == 0L }
.subscribe { printStats(it, startTime) }

average time 6ms

2.5 BUT the items are not in order!

enable log - oops! things not in order!

3. Use concat map

show its now in order

back to 6ms

4. flatmapSequential

why is this not faster?

 */
fun main(args: Array<String>) {

    val startTime: Instant = Instant.now()

    val incomingEventFlux: Flux<IncomingMsg> = incomingEvents()

    incomingEventFlux
            .onBackpressureBuffer()
            .flatMapSequential { incomingEvent ->
                enrich(incomingEvent)
                        .subscribeOn(Schedulers.elastic())
            }
            .concatMap { enrichedMsg ->
                writeToOutgoing(enrichedMsg)
                        .flatMap { commitOriginalOffset(enrichedMsg) }
            }
            .doOnNext { log(it) }
            .filter { it.id % skipCount == 0L }
            .subscribe { printStats(it, startTime) }


    while (true) {
    }
}


val skipCount = 100L

fun printStats(event: EnrichedMsg, startTime: Instant) {
    if (event.id == 0L) {
        return
    } else {
        val now = Instant.now()
        val elapsedTime = Duration.between(startTime, now).toMillis()
        val lastCount = event.id
        val timePerEvent = elapsedTime / lastCount
        log("Events processed: $lastCount Time per req: $timePerEvent ms")
    }
}

fun writeToOutgoing(e: EnrichedMsg): Flux<EnrichedMsg> = Flux.just(e)

fun commitOriginalOffset(e: EnrichedMsg): Flux<EnrichedMsg> {
    //log("Commiting ${e.id}")
    return Flux.just(e)
}

fun incomingEvents(): Flux<IncomingMsg> {
    return Flux
            .interval(Duration.ofMillis(1))
            .map { l -> IncomingMsg(l) }
}

fun enrich(incoming: IncomingMsg): Flux<EnrichedMsg> {
    return Flux.generate<EnrichedMsg> { sink ->
        //log("Making api call for ${incoming.id}")
        sink.next(EnrichedMsg(incoming.id, random8Chars()))
        Thread.sleep(delayAmount(incoming))
    }
            // .delayElements(Duration.ofMillis(delayAmount(incoming)))
            .take(1)
}


fun random8Chars(): String =
        (0..10).map {
            'a'.plus(Random().nextInt(26))
        }.joinToString("", "", "")


fun delayAmount(incoming: IncomingMsg) =
        if ((incoming.id % 2) == 0L) 70L else 10L


data class IncomingMsg(val id: Long)

data class EnrichedMsg(val id: Long, val enrichedData: String)
