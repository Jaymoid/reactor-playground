package james

import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration


fun main(args: Array<String>) {

    val iAmAMonoVoid = Mono.fromRunnable<Void> { log("hi") }
    val funToMonoVoid: (Long) -> Mono<Void> = { long -> Mono.fromRunnable { log("hi $long") } }

    Flux.interval(Duration.ofMillis(3000))
        .flatMap { long ->
            pushToQueue(long)
                .then(iAmAMonoVoid) // runs
                .then({ iAmAMonoVoid}())
                .then(funToMonoVoid(long)) // runs
                .thenEmpty(funToMonoVoid(long+2))
                .thenEmpty(funToMonoVoid(long+4))
                .thenMany(iAmAMonoVoid)
                .thenEmpty(iAmAMonoVoid)
        }
        .subscribe { log(it) }
        //.subscribe(subscriber())

    while (true) {
    }
}

private fun pushToQueue(l: Long): Mono<Void> =
    Flux.just(l)
        .flatMap {
            log("I AM PUSHING THIS TO QUEUE: $it")
            Flux.just(it)
        }.then()

private fun subscriber(): Subscriber<Void> = object : Subscriber<Void> {
    override fun onComplete() = log("Completed processing of the stream.")
    override fun onSubscribe(s: Subscription) { log("subbed")}
    override fun onNext(t: Void) { log("onNext")}
    override fun onError(t: Throwable) {
        log( "An exception prevented successful processing of the stream." )
        //shutDownApplication()
    }
}
