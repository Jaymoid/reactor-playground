package james

fun log(msg: Any) {
    System.out.printf("[%s] %s%n", Thread.currentThread().name, msg.toString())
}