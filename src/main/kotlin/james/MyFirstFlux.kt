package james

import reactor.core.publisher.Flux

fun main(args: Array<String>) {
    Flux
        .just(1, 2, 3)
        .doOnEach {signalConsumer ->  println("Each ${signalConsumer.get()}" )}
        .doOnComplete { println("complete!") }
        .map { i -> i.toString() }
        .subscribe { println(it) }


}
