package james

import org.reactivestreams.Subscription
import reactor.core.publisher.BaseSubscriber
import reactor.core.publisher.Flux
import java.time.Duration

fun main(args: Array<String>) {

    val waitThing = Object()

    Flux.interval(Duration.ofMillis(100))
        .flatMap{x ->
            log("flatMap $x")
            if (x % 4L == 0L)
                Flux.just(x)
            else
                Flux.empty()
        }
        .subscribe(object: BaseSubscriber<Long>() {

            override fun hookOnSubscribe(subscription: Subscription) {
                request(1)
            }

            override fun hookOnNext(value: Long) {
                log("Received: " + value)
                if (value < 5) {
                    request(1)
                } else {
                    log("cancelling")
                    cancel()
                }
            }

            override fun hookOnCancel() {
                super.hookOnCancel()
                notifyObj()
            }

            override fun hookOnError(throwable: Throwable) {
                super.hookOnError(throwable)
                notifyObj()
            }

            fun notifyObj() {
                synchronized(waitThing) {
                    waitThing.notify()
                }
            }

            override fun hookOnComplete() {
                log("OnComplete!")
                super.hookOnComplete()
            }
        })


    try {
        synchronized(waitThing) {
            waitThing.wait()
            log("Exiting main thread")
        }
    } catch (e: InterruptedException) {
        println(e)
    }
}
