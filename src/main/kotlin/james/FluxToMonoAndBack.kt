package james

import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration

fun main(args: Array<String>) {
    Flux
        .interval(Duration.ofMillis(1000))
        .flatMap {
            validate(it) // validation
        }
        .flatMap {
            pushToQueue(it)
                .then(commitIt(it))
                .thenMany(Flux.just(it))
        }
        .subscribe(object : Subscriber<Long> {
            var sub: Subscription? =null
            override fun onComplete() {
                log("on complete")
            }

            override fun onSubscribe(s: Subscription) {
                log("on sub")
                sub = s
                sub?.request(1)
            }

            override fun onNext(t: Long) {
                log("on next $t")
                sub?.request(1)
            }

            override fun onError(t: Throwable?) {
                log("on err $t")
            }
        })

    while (true) {
    }
}

fun commitIt(it: Long?): Mono<Void> {
    return Mono.fromRunnable{ log("Commiting $it")}
}

private fun validate(l: Long): Flux<Long> =
    Flux.just(l)
        .flatMap { thing ->
            if (thing % 2 == 0L) {
                Flux.just<Long>(thing)
            } else {
                // Eep we have hit an error
                pushToQueue(thing, true)
                    .thenMany (
                        Flux.just(108L, 208L, 808L)
                        )
            }
        }

private fun pushToQueue(l: Long, dlq: Boolean = false): Mono<Void> =
    Flux.just(l)
        .flatMap {
            val queue = if (dlq) "DL" else "OUT"
            log("I AM PUSHING THIS TO $queue QUEUE: $it")
            Flux.just(it)
        }.then()
