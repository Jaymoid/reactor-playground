package james

import reactor.core.publisher.Flux
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers
import java.time.Duration
import java.util.concurrent.ThreadFactory
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.atomic.AtomicInteger

internal fun getRandyNumbersFlux(): Flux<Int> {
    return Flux.generate { generator ->
        val randomNo = ThreadLocalRandom.current().nextInt(0, 501)
        log("generating number...$randomNo")
        generator.next(randomNo)
    }
}

fun main(args: Array<String>) {

    getRandyNumbersFlux()
            .delayElements(Duration.ofSeconds(2))
            .subscribeOn(james.newScheduler("subscribe-on-1-"))
            .publishOn(james.newScheduler("publish-on-1-"))  // Publish on GOES DOWN vvv
            .map { i ->
                log("map 1...")
                i
            }
            .subscribeOn(james.newScheduler("subscribeOn-2-")) // subscribeOn placement in pipeline does not matter... subscribeOn = where source elements are omitted from.
            // Subscribe on GOES UP! ^^^^
            .map { i ->
                log("map 2...");
                i;
            }
            .publishOn(james.newScheduler("publish-on-1-"))
            .map { i ->
                log("map 3...");
                i;
            }
            .publishOn(newScheduler("publish-on-2-"))
            .map { i ->
                log("map 4...");
                i
            }
            .map { i ->
                log("map 5...");
                i
            }
            .subscribe { i -> log("subscribed $i") }

    while (true) {
    }
}



internal fun newScheduler(threadName: String): Scheduler {
    return Schedulers.newSingle(SimpleThreadFactory(threadName))
}

class SimpleThreadFactory(var threadName: String) : ThreadFactory {
    var threadCount = AtomicInteger(0)

    override fun newThread(r: Runnable): Thread {
        return Thread(r, threadName + threadCount.getAndIncrement())
    }
}
