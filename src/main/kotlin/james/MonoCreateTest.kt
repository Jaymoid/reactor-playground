package james

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration

fun main(args: Array<String>) {

    val mono = Mono.create<Int> { emitter ->
        try {
            println("hello")
            emitter.success(1)
        } catch (e: Exception) {
            emitter.error(e)
        }
    }

    val m = Mono.create<Void> { emitter ->
        try {
            println("hello")
            emitter.success()
        } catch (e: Exception) {
            emitter.error(e)
        }
    }

    println("one line before invoking mono")

    mono

    println("one line after invoking mono")

    //mono.subscribe{ println("it: $it") }

    Flux.interval(Duration.ofSeconds(1))
        .flatMap {long ->
            println("in flatmap start $long")

            Flux.create<Long> { emitter ->
                println("in flux create $long")
                emitter.next(long)
            }
        }
        .take(3)
        .thenEmpty(m)
        .subscribe { println("it $it") }

    while (true) {
    }

}
