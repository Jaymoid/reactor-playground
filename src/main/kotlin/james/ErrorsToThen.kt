package james

import org.reactivestreams.Subscription
import reactor.core.publisher.BaseSubscriber
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration

fun main(args: Array<String>) {


    Flux.interval(Duration.ofMillis(1000))
    //Flux.error<Long>(Exception("boourns"))
        .take(2)
        .thenEmpty(Mono.create {
            log("invoked then mono")
            //it.error(Exception("shit"))
            it.success()
        })
        .doOnSuccess { log("success state thing") }
        .doOnError { log("errored state thing") }
        .subscribe(object : BaseSubscriber<Void>() {

            override fun hookOnSubscribe(subscription: Subscription) {
                request(1)
            }

            override fun hookOnNext(value: Void) {
                log("Received: " + value)
            }

            override fun hookOnComplete() {
                log("OnComplete!")
                super.hookOnComplete()
            }

            override fun hookOnError(throwable: Throwable) {
                log("OnError!")
                // super.hookOnError(throwable)
            }
        })
    while (true) {
    }
}
