package james

import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import reactor.core.publisher.BaseSubscriber
import reactor.core.publisher.Flux
import java.time.Duration

fun main(args: Array<String>) {

    val waitThing = Object()

    Flux.interval(Duration.ofMillis(100))
        .subscribe(object: BaseSubscriber<Long>() {

            override fun hookOnSubscribe(subscription: Subscription) {
                request(1)
            }

            override fun hookOnNext(value: Long) {
                System.out.println("Received: " + value)
                if (value < 5)
                    request(1)
                else
                    cancel()
            }

            override fun hookOnCancel() {
                super.hookOnCancel()
                notifyObj()
            }

            override fun hookOnError(throwable: Throwable) {
                super.hookOnError(throwable)
                notifyObj()
            }

            fun notifyObj() {
                synchronized(waitThing) {
                    waitThing.notify()
                }
            }

            override fun hookOnComplete() {
                log("OnComplete!")
                super.hookOnComplete()
            }
        })


    try {
        synchronized(waitThing) {
            waitThing.wait()
            println("Exiting main thread")
        }
    } catch (e: InterruptedException) {
        println(e)
    }

}
