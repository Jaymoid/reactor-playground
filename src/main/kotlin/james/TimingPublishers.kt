package james

import reactor.core.publisher.Flux
import reactor.util.function.component1
import reactor.util.function.component2
import java.time.Clock
import java.time.Duration
import java.time.Instant

fun main(args: Array<String>) {

    fluxOfStarttime()
        .doOnNext { log(it)  }
        .subscribe()

//    log("Hi")
//
//    val flux = Flux.interval(Duration.ofSeconds(3), Duration.ofSeconds(1))
//
//    timedFlux(flux)
//        .take(5)
//        .doOnEach { log(it) }
//        .subscribe()
//
//    log("Hi again")

    while (true) {
    }
}

fun <T> timedFlux(fluxToTime: Flux<T>): Flux<TimedElement<T>> {

    return Flux.just(instantNow())
        .flatMap { startInstant -> fluxToTime.zipWith(Flux.generate<Instant> { it.next(startInstant) }) }
        .map { (element: T, start: Instant) ->
            TimedElement(element, Duration.between(start, instantNow()))
        }
}


fun fluxOfStarttime() :Flux<Instant> {
    val startTime = instantNow()
    return Flux.generate { it.next(startTime) }
}
fun instantNow() = Clock.systemDefaultZone().instant()

data class TimedElement<T>(
    val element: T,
    val duration: Duration
)
