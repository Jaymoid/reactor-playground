package james

import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import reactor.core.Exceptions
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration

fun main(args: Array<String>) {


    Flux.interval(Duration.ofSeconds(1))
        .concatMap {
            storeInDB(it)
                .thenEmpty(commit())
                .doOnError { log("hello from do on error!") }
        }
        .subscribe(object : Subscriber<Void> {
            var sub: Subscription? = null
            override fun onComplete() {
                log("on complete")
            }

            override fun onSubscribe(s: Subscription) {
                log("on sub")
                sub = s
                sub?.request(1)
            }

            override fun onNext(t: Void) {
                log("on next $t")
                sub?.request(1)
            }

            override fun onError(t: Throwable?) {
                log("on err $t")
            }
        })


    while(true) {}

}

fun storeInDB(long: Long): Mono<Void> {
    log("Successfully processed person with id $long from Kafka")
    //if ((long.toInt() % 2) == 0)
        //throw Exception("shit on it $long")
    return Mono.empty()
}

var commitcount =0
fun commit(): Mono<Void> {
    log("committing")
    commitcount++
    if (commitcount > 3)
         return Mono.error<Void>(Exception("commit fail"))
        //throw Exception("commit fails")
    return Mono.empty()
}
